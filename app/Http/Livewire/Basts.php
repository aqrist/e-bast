<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Bast;

class Basts extends Component
{
    public $basts, $nama_pihak_1, $jabatan_pihak_1, $nama_pihak_2, $jabatan_pihak_2, $status, $bast_id;
    public $isModal = 0;


    public function render()
    {
        $this->basts = Bast::orderBy('created_at', 'DESC')->get();
        return view('livewire.bast.basts');
    }

    public function create()
    {
        $this->resetFields();
        $this->openModal();
    }

    public function closeModal()
    {
        $this->isModal = false;
    }

    public function openModal()
    {
        $this->isModal = true;
    }

    public function resetFields()
    {
        $this->nama_pihak_1 = '';
        $this->jabatan_pihak_1 = '';
        $this->nama_pihak_2 = '';
        $this->jabatan_pihak_2 = '';
        $this->status = '';
        $this->bast_id = '';
    }

    public function store()
    {
        $this->validate([
            'nama_pihak_1' => 'required|string',
            'jabatan_pihak_1' => 'required|string',
            'nama_pihak_2' => 'required|string',
            'jabatan_pihak_2' => 'required|string',
            'status' => 'required'
        ]);

        Bast::updateOrCreate(
            [
                'id' => $this->bast_id
            ],
            [
                'nama_pihak_1' => $this->nama_pihak_1,
                'jabatan_pihak_1' => $this->jabatan_pihak_1,
                'nama_pihak_2' => $this->nama_pihak_2,
                'jabatan_pihak_2' => $this->jabatan_pihak_2,
                'status' => $this->status
            ]);

        session()->flash('message', $this->bast_id ? 'BAST diperbaharui' : 'BAST ditambahkan');
        $this->closeModal();
        $this->resetFields();
    }

    public function edit($id)
    {
        $bast = Bast::find($id);

        $this->bast_id = $id;
        $this->nama_pihak_1 = $bast->nama_pihak_1;
        $this->jabatan_pihak_1 = $bast->jabatan_pihak_1;
        $this->nama_pihak_2 = $bast->nama_pihak_2;
        $this->jabatan_pihak_2 = $bast->jabatan_pihak_2;
        $this->status = $bast->status;

        $this->openModal();
    }

    public function delete($id)
    {
        $bast = Bast::find($id);

        $bast->delete();
        session()->flash('message'. 'BAST dihapus');
    }
}
