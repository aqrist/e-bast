<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bast extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_pihak_1',
        'jabatan_pihak_1',
        'nama_pihak_2',
        'jabatan_pihak_2',
        'created_by',
        'status'];
}
